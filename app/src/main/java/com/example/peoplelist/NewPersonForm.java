package com.example.peoplelist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewPersonForm extends AppCompatActivity {

    Button btn_ok, btn_cancel;
    EditText et_name, et_age, et_pictureNumber;

    int positionToEdit = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_person_form);

        btn_ok = findViewById(R.id.btn_ok);
        btn_cancel = findViewById(R.id.btn_cancel);
        et_name = findViewById(R.id.et_name);
        et_age = findViewById(R.id.et_age);
        et_pictureNumber = findViewById(R.id.et_picturenumber);

        //listen for incoming changes
        Bundle incomingMessages = getIntent().getExtras();

        if(incomingMessages != null){

            //capture incoming data
            String name = incomingMessages.getString("name");
            int age = incomingMessages.getInt("age");
            int pictureNumber = incomingMessages.getInt("pictureNumber");
            positionToEdit = incomingMessages.getInt("edit");

            et_name.setText(name);
            et_age.setText(Integer.toString(age));
            et_pictureNumber.setText(Integer.toString(pictureNumber));

        }

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newName = et_name.getText().toString();
                String newAge = et_age.getText().toString();
                String newPictureNumber = et_pictureNumber.getText().toString();

                Intent i = new Intent(v.getContext(), MainActivity.class);

                i.putExtra("edit", positionToEdit);
                i.putExtra("name", newName);
                i.putExtra("age", newAge);
                i.putExtra("pictureNumber", newPictureNumber);

                startActivity(i);
            }
        });
    }
}
