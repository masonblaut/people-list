package com.example.peoplelist;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.peoplelist.R;

class PersonAdapter extends BaseAdapter {


    Activity mActivity;
    MyFriends myFriends;

    public PersonAdapter(Activity mActivity, MyFriends myFriends) {
        this.mActivity = mActivity;
        this.myFriends = myFriends;
    }

    //tells me how many people are in the list
    @Override
    public int getCount() {
        return myFriends.getMyFriendsList().size();
    }

    //will return a person
    @Override
    public Person getItem(int position) {
        return myFriends.getMyFriendsList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View onePersonLine;
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        onePersonLine = inflater.inflate(R.layout.person_one_line, parent, false);

        TextView tv_name = onePersonLine.findViewById(R.id.tv_name);
        TextView tv_age = onePersonLine.findViewById(R.id.tv_agevalue);
        ImageView iv_icon = onePersonLine.findViewById(R.id.iv_icon);

        Person p = this.getItem(position);

        tv_name.setText(p.getName());
        tv_age.setText(Integer.toString(p.getAge()));

        int icon_resource_numbers [] = {
                R.drawable.bluedress,
                R.drawable.bowtie,
                R.drawable.brownhair,
                R.drawable.businesssuit,
                R.drawable.constructionworker,
                R.drawable.fancydress,
                R.drawable.fancysuit,
                R.drawable.farmer,
                R.drawable.girlglasses,
                R.drawable.greenstripes,
                R.drawable.greyhat,
                R.drawable.greyshirt,
                R.drawable.guyglasses,
                R.drawable.reddress,
                R.drawable.redhead,
                R.drawable.suitandtie
        };
        iv_icon.setImageResource(icon_resource_numbers[position]);

        return onePersonLine;
    }
}
